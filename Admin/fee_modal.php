

    <div class="modal fade" id="feeModal<?php echo $row['fee_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="feeModal"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="feeModal">fee Detail</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body m-4">
                    

                    <form class="row g-2" action="">
                        <div class="col-md-12">
                            <label for="inputEmail4" class="form-label">Date Created</label>
                            <input type="date" class="form-control " value="<?php echo isset($row['fee_id']) ? $row['fee_date'] : '' ?>" id="inputEmail4">
                        </div>


                        <div class="col-md-12">
                            <label for="inputEmail4" class="form-label mt-3">Arrival</label>
                            <textarea type="text" class="form-control" id="inputPassword4" rows="3" name="fee_description"><?php echo isset($row['fee_id']) ? $row['fee_desc'] : '' ?></textarea>
                        </div>

                        <div class="col-md-6">
                            <label for="inputEmail4" class="form-label mt-3">Fee Status</label>
                            <select id="parent_state" class="form-control">
                            <option >Choose..</option>
                            <option value="PAID" <?php echo $row['fee_status'] == "PAID" ?  'selected="selected"': '' ?>>Paid</option>
                            <option value="UNPAID" <?php echo $row['fee_status'] == "UNPAID" ? 'selected="selected"' : '' ?>>Unpaid</option>
                        </select>
                        </div>

                        <div class="col-md-6">
                            <label for="inputEmail4" class="form-label mt-3">Fee Amount</label>
                            <input type="text" class="form-control " value="RM <?php echo isset($row['fee_id']) ? number_format($row['fee_amount'],2) : '' ?>" id="inputEmail4">                    

                        </div>

                        <div class="col-md-12 mt-3">
                            <hr class="solid">
                           <h5 class="font-weight-bold">Child Information</h5>
                        </div>


                        


                    </form>

                    <fieldset disabled class="row g-2">
                        <div class="col-md-6">
                            <label for="inputEmail4" class="form-label">Name</label>
                            <input type="text" class="form-control"  value="<?php echo isset($row['fee_id']) ? $row['child_name'] : '' ?>" id="disabledTextInput">
                        </div>

                        <div class="col-md-6">
                            <label for="inputEmail4" class="form-label">Group</label>
                            <input type="text" class="form-control " value="<?php echo isset($row['fee_id']) ? $row['group_name'] : '' ?>" id="disabledTextInput">
                        </div>
                    </fieldset>


          
                </div>
                <div class="modal-footer ">
                   
                    <button class="btn btn-danger" type="button" data-dismiss="modal">Delete</button>
                    <button class="btn btn-success" type="button" data-dismiss="modal">Save</button>
                </div>
            </div>
        </div>
</div>

<!-- set attendance Modal-->
<div class="modal fade" id="setFeeModal" tabindex="-1" role="dialog" aria-labelledby="setFeeModal"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">

    <form  action="handler/fee_handler.php"  method="GET">
        <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="feeModal">Fee Detail</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="row g-2 modal-body m-4">

                    
                        <div class="col-md-12">
                            
                            <label for="inputPassword4" class="form-label">Group</label>
                            <select id="group_id" name="group_id" class="form-control">
                            <option selected>Choose..</option>
                            <?php while($row=$group->fetch_assoc()): ?>
                            <option value="<?php echo $row['group_id'] ?>"><?php echo $row['group_name'] ?></option>
                            <?php endwhile; ?>
                            </select>
                        </div>

                        

                        <div class="col-md-12">
                            <label for="inputEmail4" class="form-label mt-3">Description</label>
                            <textarea type="text" class="form-control" id="inputPassword4" rows="3" name="fee_description"></textarea>
                        </div>

                    
                </div>

                <div class="modal-footer">
                     
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-success" type="submit"  value="submit">Create</button>
                </div>
            
            </div>
        </form>
    </div>
</div>


