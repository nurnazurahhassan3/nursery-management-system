
<!-- attendance Info Modal-->
<div class="modal fade" id="attendanceModal<?php echo isset($row['attendance_id'])? $row['attendance_id']:'' ?>" tabindex="-1" role="dialog" aria-labelledby="attendanceModal"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="attendanceModal">Attendance Detail</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form  action="handler/attendance_handler.php" method='POST'>
                <div class="modal-body  row g-2 m-4">
                    

                    
                        <div class="col-md-12">
                            <input type="hidden" class="form-control " value="<?php echo isset($row['attendance_id']) ? $row['attendance_id'] : '' ?>" name="attendance_id">
                            <label for="inputEmail4" class="form-label">Date</label>
                            <input type="date" class="form-control " value="<?php echo isset($row['attendance_id']) ? $row['attendance_date'] : '' ?>" name="attendance_date">
                        </div>

                        <div class="col-md-12 mt-4 ">
                        
                        <h5 class="font-weight-bold">Time:</h5>
                        </div>

                        <div class="col-md-6">
                            <label for="inputEmail4" class="form-label">Arrival</label>
                            <input type="time" class="form-control " value="<?php echo isset($row['attendance_id']) ? $row['attendance_arrivalTime'] : '' ?>"  name="attendance_arrivalTime">
                        </div>

                        <div class="col-md-6">
                            <label for="inputEmail4" class="form-label">Pick Up</label>
                            <input type="time" class="form-control " value="<?php echo isset($row['attendance_id']) ? $row['attendance_pickupTime'] : '' ?>" name="attendance_pickupTime">
                        </div>

                    

                        

                        <div class="col-md-12 mt-3">
                            <hr class="solid">
                        <h5 class="font-weight-bold">Child Information</h5>
                        </div>


                        


                    

                        <fieldset disabled  class="col-md-6">
                            
                                <label for="inputEmail4" class="form-label">Name</label>
                                <input type="text" class="form-control"  value="<?php echo isset($row['attendance_id']) ? $row['child_name'] : '' ?>" id="disabledTextInput">
                            
                        </fieldset>
                        <fieldset disabled  class="col-md-6">
                            
                                <label for="inputEmail4" class="form-label">Group</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['attendance_id']) ? $row['group_name'] : '' ?>" id="disabledTextInput">
                        
                        </fieldset>


        
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" type="button" data-dismiss="modal">Delete</button>
                    <button class="btn btn-success" type='submit'>Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- set attendance Modal-->
<div class="modal fade" id="setPickUpModal<?php echo isset($row['attendance_id'])? $row['attendance_id']:''?>" tabindex="-1" role="dialog" aria-labelledby="setPickUpModal"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">

        <form  action="handler/attendance_handler.php"  method="POST">
            <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="attendanceModal">Pick Up Detail</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="row g-2 modal-body m-4">
                    <input type="text" class="form-control " value="<?php echo isset($row['attendance_id']) ? $row['attendance_id'] : '' ?>" name="attendance_id">

                        <fieldset disabled class="col-md-12">

                                <label for="inputEmail4" class="form-label">Name</label>
                                <input type="text" class="form-control"  value="<?php echo isset($row['attendance_id']) ? $row['child_name'] : '' ?>" id="disabledTextInput">
                        
                        </fieldset>

                            <fieldset disable class="col-md-12">
                                <label for="inputEmail4" class="form-label mt-3">Date</label>
                                <input type="date" class="form-control " value="<?php echo isset($row['attendance_id']) ? $row['attendance_date']: '' ?>" id="attendance_date">
                            </fieldset>

                            <div class="col-md-12 mt-4 ">
                            
                            <h5 class="font-weight-bold">Time:</h5>
                            </div>

                            <fieldset disabled class="col-md-6">
                                <label for="inputEmail4" class="form-label">Arrival</label>
                                <input type="time" class="form-control " value="<?php echo isset($row['attendance_id']) ? $row['attendance_arrivalTime'] : '' ?>" id="attendance_arrivalTime">
                            </fieldset>

                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">Pick Up</label>
                                <input type="time" class="form-control " value="<?php echo isset($row['attendance_id']) ? $row['attendance_pickupTime'] : '' ?>" name="attendance_pickupTime">
                            </div>

                            <div class="col-md-12 mt-4 ">
                            
                            <h5 class="font-weight-bold">Pick Up Details:</h5>
                            </div>

                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">Name</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['pickup_id']) ? $row['pickup_name'] : '' ?>" name="pickup_name">
                            </div>

                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">Phone Number</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['pickup_id']) ? $row['pickup_phoneNum'] : '' ?>" name="pickup_phoneNum">
                            </div>

                            <div class="col-md-12">
                                <label for="parent_state" class="form-label mt-3">Relationship</label>
                                <select id="parent_state" class="form-control " name='pickup_relationship'>
                                    <option selected><?php echo isset($row['pickup_id']) ? ucwords(strtolower ($row['pickup_relationship'] )): '' ?></option>
                                    <option value='Sister'>...</option>
                                </select>
                            </div>

                        
                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-success" type="submit"  value="submit">Save</button>
                    </div>
                
                </div>
        </form>
    </div>
</div>

<!-- alert modal-->
<div class="modal align-middle fade" id="alertModal<?php echo $row['attendance_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                
                <div class="modal-body text-center my-3">
                    <div class="my-2">
                        <h1 class="my-4"><svg xmlns="http://www.w3.org/2000/svg"  height="60" fill="grey" class="bi bi-exclamation-circle" viewBox="0 0 16 16">
        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
        <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
        </svg></i></h1>
                                <h4> <b>There is no arrived time has been set! </b></h4>       
                        <p> Please set the arrived time before you want to insert the pick up details</p>
                    </div>

                    <div>
                    <button class="btn btn-success" type="button" data-dismiss="modal">Okay</button>
                    
                    </div>

                </div>
                
                
            </div>
        </div>
</div>





<!-- set attendance Modal-->
<div class="modal fade" id="setAttendanceModal" tabindex="-1" role="dialog" aria-labelledby="setattendanceModal"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">

    <form  action="handler/attendance_handler.php"  method="GET">
        <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="attendanceModal">Attendance Detail</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="row g-2 modal-body m-4">

                    
                        <div class="col-md-12">
                            <label for="inputPassword4" class="form-label">Group</label>
                            <select id="group_id" name="group_id" class="form-control">
                            <option selected>Choose..</option>
                            <?php while($row=$group->fetch_assoc()): ?>
                            <option value="<?php echo $row['group_id'] ?>"><?php echo $row['group_name'] ?></option>
                            <?php endwhile; ?>
                            </select>
                        </div>

                        <div class="col-md-12">
                            <label for="inputEmail4" class="form-label mt-3">Date</label>
                            <input type="date" class="form-control " id="attendance_date" name="attendance_date">
                        </div>

                    
                </div>

                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-success" type="submit"  value="submit">Create</button>
                </div>
            
            </div>
        </form>
    </div>
</div>





