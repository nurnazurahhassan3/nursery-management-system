<html lang="en">
<?php
        
        include('../db_connect.php');
?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../image/logo1.png" >
    <title>Management - Payment</title>

    <?php include "header.php"?>
    

</head>
<body id="page-top">


     <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-success sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand  align-items-center justify-content-center mb-4" href="dashboard.php">
            <div class="sidebar-brand-icon ">
            <img class="mb-2" src="../image/logo1.png" width="75"style="-webkit-filter: drop-shadow(5px 5px 5px #666666);
                    filter: drop-shadow(5px 5px 5px #666666);" alt="">
            </div>
            <div class="sidebar-brand-text ">Taska Ummi Sakiza </div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item  ">
            <a class="nav-link " href="dashboard.php">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>

        <!-- Divider (line) -->
        <hr class="sidebar-divider my-0">


        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item ">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-scroll"></i>
                <span>Registration</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded"> 
                    <a class="collapse-item " href="new_register.php">Registration</a>
                    <a class="collapse-item" href="rejected_register.php">Rejected Registration</a>
                </div>
            </div>
        </li>



        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Information
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item ">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                aria-expanded="true" aria-controls="collapsePages">
                <i class="fas fa-child"></i>
                <span>Children</span>
            </a>
            <div id="collapsePages" class="collapse " aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                
                    <a class="collapse-item" href="child_list.php">List</a>
                   <a class="collapse-item " href="child_attendance.php">Attendance</a>

                </div>
            </div>
        </li>

        <!-- Nav Item - Charts -->
        <li class="nav-item parent" >
            <a class="nav-link parent_btn" href="parents.php">
            <i class="fas fa-user-friends"></i>
            <span>Parent</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
        Management
        </div>

        <!-- Nav Item - Charts -->
        <li class="nav-item">
            <a class="nav-link" href="mgt_group.php">
            <i class="fa-solid fa-chalkboard-user"></i>
            <span>Group</span></a>
        </li>

        <li class="nav-item active">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree"
                aria-expanded="true" aria-controls="collapseThree">
                <i class="fas fa-money-bill"></i>
                <span>Financial</span>
            </a>
            <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                
                    <a class="collapse-item " href="mgt_fee.php">Fee</a>
                    <a class="collapse-item active" href="mgt_payment.php">Payment</a>

                </div>
            </div>
        </li>

        <!-- Nav Item - Charts -->
        <li class="nav-item">
            <a class="nav-link" href="mgt_group.php">
            <i class="fas fa-sticky-note"></i>
            <span>Report</span></a>
        </li>

        <!-- Nav Item - Charts -->
        <li class="nav-item">
            <a class="nav-link" href="mgt_staff.php">
            <i class="fas fa-user-edit"></i>
            <span>Staff</span></a>
        </li>




        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>



        </ul>
        <!-- End of Sidebar -->
         
       
        


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column"> 

        <!-- Main Content -->
        <div id="content"> 

            <!-- Topbar -->
            <?php include "topbar.php"; ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">
                
                    <?php 
                    
                    
                    $payment= $conn->query("SELECT * from 
                    payment m 
                    JOIN fee f ON m.fee_id = f.fee_id
                    JOIN child c ON c.child_id = f.child_id  
                   
                    JOIN parent p ON p.parent_id = c.parent_id  
                    order by m.payment_id asc");
                    ?>  

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        
                        <h1 class="h3 mb-0 text-gray-800">Payment</h1>
                       
                           
                    </div>

                  
                        

                    <!--Table-->
                    <div class="card shadow mb-4 ">
                        

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr class="thead-light">
                    
                                            <th class="col-1">#</th>
                                            <th class="col-2">Date</th>
                                            <th class="col-2">Fee ID</th>
                                            <th class="col-2">Payment Amount</th>
                                            <th class="col-2">Action</th>
                                        </tr>
                                    </thead>

                                


                                    <tbody>
                                        <?php while($row=$payment->fetch_assoc()): ?>
                                        <tr>
                                        
                                            <td><?php echo $row['payment_id'] ?></td>
                                            <td><?php echo $row['payment_date'] ?></td>
                                            <td>
                                            <?php echo $row['fee_id'] ?>
                                            </td>
                                            <td><?php echo $row['payment_amount'] ?></td>
                                            <td>
                                                <button class="btn btn-primary" href="#" data-toggle="modal" data-target="#paymentModal<?php echo $row['payment_id'] ?>"><a>View</a></button>
                                                <!-- Set fee Modal-->
                                                <?php include "payment_modal.php"; ?>
                                                <a href="pdf_generator.php?id=<?php echo $row['fee_id'] ?>" target="_blank"><button class="btn btn-outline-primary" type="submit">Invoice</button></a>
                                               
                                                
                                            </td>
                                        </tr>
                                        <?php endwhile; ?>
                                    
                                    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    

                

            </div>
            <!-- End Page Content -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php include "footer.php"; ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
     <!-- End of Page Wrapper -->

        <!-- signout Modal-->
        <?php include "signout.php"; ?>

        

      <!-- Script-->
      <?php include "script.php"; ?>

  
      


    
</body>
</html>