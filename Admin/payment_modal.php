

    <div class="modal fade" id="paymentModal<?php echo isset($row['payment_id'])?  $row['payment_id'] :$row['fee_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="paymentModal"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="paymentModal"><?php echo isset($row['payment_id']) ? "Payment for Invoice <b>#".$row['fee_id']. "</b>": 'Payment' ?></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body m-4">
                    

                    <form class="row g-2" action="" >
                     
                        <div class="col-md-12">
                            <input type="hidden" class="form-control "  value="<?php echo $row['fee_id'] ?>"  id="inputEmail4">
                            <label for="inputEmail4" class="form-label">Date</label>
                            <input type="date" class="form-control " value="<?php echo isset($row['payment_id']) ? $row['payment_date'] : '' ?>" id="inputEmail4">
                        </div>

                        <div class="col-md-6">
                            <label for="inputEmail4" class="form-label mt-3">Payment Method</label>
                            <select id="parent_state" class="form-control">
                            <option >Choose..</option>
                            <option value="CASH" <?php echo $row['payment_type'] == "CASH" ?  'selected="selected"': '' ?>>Cash</option>
                            <option value="ONLINE BANKING" <?php echo $row['payment_type'] == "ONLINE BANKING" ? 'selected="selected"' : '' ?>>Online Banking</option>
                        </select>
                        </div>

                        <div class="col-md-6">
                            <label for="inputEmail4" class="form-label mt-3">payment Amount</label>
                            <input type="text" class="form-control " value=" <?php echo isset($row['payment_id']) ? "RM".number_format($row['payment_amount'],2) : '' ?>" id="inputEmail4">                    

                        </div>

                        <div class="col-md-12">
                        <label for="health_information" class="form-label mt-3">Payment Proof</label>
                        <input type="file" class="form-control-file" value="<?php isset($row['payment_id']) ? $row['payment_proof'] : '' ?>" id="health_information">
                        
                        <a href="download.php?id=<?php echo $row['child_id'] ?>"> download</a>
                       
                    </div>

                       
                        


                    </form>
                    
                    <fieldset disabled class="row g-2">

                        <?php if(isset($row['payment_id'])):?>

                        <div class="col-md-12 mt-3">
                            <hr class="solid">
                        <h5 class="font-weight-bold">Payment By:</h5>
                        </div>

                        <div class="col-md-6">
                            <label for="inputEmail4" class="form-label mt-3">Name</label>
                            <input type="text" class="form-control " value=
                            "<?php 
                                if (isset($row['payment_id'])){ 
                                    if($row['father_name']=="" || $row['father_name']==NULL )
                                        echo $row['father_name'];
                                    else
                                         echo $row['mother_name'] ; 
                                    }
                                else
                                echo "";
                            ?>" 
                            id="inputEmail4">                    

                        </div>

                        <div class="col-md-6">
                            <label for="inputEmail4" class="form-label mt-3">Phone Number</label>
                            <input type="text" class="form-control " value="
                            <?php 
                                if (isset($row['payment_id'])){ 
                                    if($row['father_phoneNum']=="" || $row['father_phoneNum']==NULL )
                                        echo $row['father_phoneNum'];
                                    else
                                         echo $row['mother_phoneNum'] ; 
                                    }
                                else
                                echo "";
                            ?>
                            " id="inputEmail4">                    

                        </div>
                        <?php endif ?>
                        
                    </fieldset>


          
                </div>
                <div class="modal-footer">

                <?php 
                if (isset($row['payment_id'])){

                  
                            
                    echo "<button class='btn btn-danger' type='button' data-dismiss='modal'>Delete</button>";
                    echo "<button class='btn btn-success' type='button' data-dismiss='modal'>Save</button>";
                     

                }
                 else{
                    echo " <button class='btn btn-secondary' data-dismiss='modal' >Cancel</button>";
                    echo "<button class='btn btn-success' type='button' type='submit'>Create</button>" ;
                 }?>
            </div>
            
            </div>
        </div>
</div>

