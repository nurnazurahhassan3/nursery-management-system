
    
    <div class="modal fade" id="parentsModal<?php echo isset($row['parent_id']) ? $row['parent_id'] : '' ?>" tabindex="-1" role="dialog" aria-labelledby="parentsModal"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="parentsModal">Parents Detail</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                
                <form  action="handler/parent_handler.php" method="POST">
                    <input type="hidden" class="form-control "  value="<?php echo isset($row['parent_id']) ? $row['parent_id'] : '' ?>" name="parent_id">

                    

                    
                      
                        
                            <div class="father" >

                                <div class="row g-2 mx-2">

                                    <div class="col-md-12 pt-4">
                                        <h5 class="text-dark "><u>Father's Detail</u></h5>
                                    </div>
                                    

                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="form-label mt-3 ">Name</label>
                                        <input type="text" class="form-control "  value="<?php echo isset($row['parent_id']) ? $row['father_name'] : '' ?>" name="father_name">
                                    </div>

                                
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label mt-3">Identity Card Number</label>
                                        <input type="text" class="form-control "value="<?php echo isset($row['parent_id']) ? $row['father_IC'] : '' ?>"  name="father_IC">
                                    </div>

                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label mt-3">Phone Number</label>
                                        <input type="text" class="form-control " value="<?php echo isset($row['parent_id']) ? $row['father_phoneNum'] : '' ?>" name="father_phoneNum">
                                    </div>

                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="form-label mt-3">Work</label>
                                        <input type="text" class="form-control " value="<?php echo isset($row['parent_id']) ? $row['father_work'] : '' ?>" name="father_work">
                                    </div>

                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="form-label mt-3">Work Address</label>
                                        <input type="text" class="form-control " value="<?php echo isset($row['parent_id']) ? $row['father_workAddress'] : '' ?>" name="father_workAddress">
                                    </div>
                                    
                                </div>
                                
                            </div>

                            <div class="mother" >

                                

                                <div class="row g-2 mx-2">

                                    <div class="col-md-12 pt-4">
                                        <h5 class="text-dark "><u>Mother's Detail</u></h5>
                                    </div>

                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="form-label mt-3 ">Name</label>
                                        <input type="text" class="form-control "  value="<?php echo isset($row['parent_id']) ? $row['mother_name'] : '' ?>" name="mother_name">
                                    </div>

                                
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label mt-3">Identity Card Number</label>
                                        <input type="text" class="form-control "value="<?php echo isset($row['parent_id']) ? $row['mother_IC'] : '' ?>"  name="mother_IC">
                                    </div>

                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label mt-3">Phone Number</label>
                                        <input type="text" class="form-control " value="<?php echo isset($row['parent_id']) ? $row['mother_phoneNum'] : '' ?>" name="mother_phoneNum">
                                    </div>

                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="form-label mt-3">Work</label>
                                        <input type="text" class="form-control " value="<?php echo isset($row['parent_id']) ? $row['mother_work'] : '' ?>" name="mother_work">
                                    </div>

                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="form-label mt-3">Work Address</label>
                                        <input type="text" class="form-control " value="<?php echo isset($row['parent_id']) ? $row['mother_workAddress'] : '' ?>" name="mother_workAddress">
                                    </div>

                                </div>

                            </div>

                            <div class="address py-4" >

                                <div class="row g-2 mx-2">

                                    <div class="col-md-12 pt-4">
                                        <h5 class="text-dark "><u>Home Address</u></h5>
                                    </div>

                                    <div class="col-md-12">
                                        <label for="inputEmail4" class="form-label mt-2">Address</label>
                                        <input type="text" class="form-control " value="<?php echo isset($row['parent_id']) ? $row['parent_address'] : '' ?>" name="parent_address">
                                    </div>

                                    <div class="col-md-4">
                                        <label for="inputEmail4" class="form-label mt-3">Town</label>
                                        <input type="text" class="form-control " value="<?php echo isset($row['parent_id']) ? $row['parent_town'] : '' ?>" name="parent_town">
                                    </div>

                                    
                                    <div class="col-md-4">
                                        <label for="inputEmail4" class="form-label mt-3">Postcode</label>
                                        <input type="text" class="form-control " value="<?php echo isset($row['parent_id']) ? $row['parent_postcode'] : '' ?>"  name="parent_postcode">
                                    </div>

                                    
                                    <div class="col-md-4">
                                        <label for="inputEmail4" class="form-label mt-3">State</label>
                                        <select id="inputState" class="form-control">
                                            <option selected><?php echo isset($row["parent_id"]) ? ucwords(strtolower($row["parent_state"]))  : 'Choose..' ?></option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            
                            
                        
                    
      
                

                    <div class="modal-footer ">
                    <a class="btn btn-danger " href="handler/parent_handler.php?delete=<?php echo $row['parent_id'] ?>">Delete</a>
                        <button class="btn btn-success" type="submit" >Save</button>
                    </div>

                </form>
               
            </div>
        </div>
    </div>

    