<?php $group = $conn->query("SELECT * from child_group "); ?>
<!-- Child Info Modal-->
<?php if(isset($row['child_id'])):?>

    <div class="modal fade" id="childModal<?php echo isset($row['child_id']) ? $row['child_id'] : '' ?>" tabindex="-1" role="dialog" aria-labelledby="childInfoModal"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
    
            <div class="modal-content" id="childDetail">

                <form action="handler/child_handler.php">
                    <div class="modal-header"> 
                            <h5 class='modal-title' id='childInfoModal'>Children Information</h5>

                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body m-4 row g-2"   >

                            <div class="col-md-12 text-center ">
                                <?php 
                                if (isset($row['child_id'])){
                                ?>
                                <img src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($row['child_image']); ?>" class="rounded mx-auto d-block mb-3" alt="..." width="200">
                                <?php }
                                
                                else{
                                ?>
                                <img src="../img/undraw_profile.svg" class="rounded mx-auto d-block mb-3" alt="..." width="200">
                                <?php }?>

                            </div>

                            <div class="col-md-11 my-4 ">
                                <label for="child_img">Children Picture</label>
                                <input type="file" class="form-control-file" id="child_img">
                            </div>
                            

                            <!--Child part-->
                            <div class="col-md-6">
                                <label for="child_name" class="form-label">Full Name</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['child_name'] : '' ?>" id="child_name">
                            </div>

                            <div class="col-md-6">
                                <label for="child_myKidNum" class="form-label ">MyKid Number</label>
                                <input type="text" class="form-control" value="<?php echo isset($row['child_id']) ? $row['child_myKidNum'] : '' ?>" id="child_myKidNum">
                            </div>

                            <div class="col-md-6">
                                <label for="child_nickname" class="form-label mt-3">Nickname</label>
                                <input type="text" class="form-control" value="<?php echo isset($row['child_id']) ? $row['child_nickname'] : '' ?>" id="child_nickname">
                            </div>

                            <div class=" col-md-4" >
                                
                                <label for="child_gender" class="form-label mt-3">Gender</label>
                                <select id="child_gender" class="form-control"  >
                                    <option >Choose..</option>
                                    <option value="MALE" <?php echo $row['child_gender'] == "MALE" ?  'selected="selected"': '' ?>>Male</option>
                                    <option value="FEMALE" <?php echo $row['child_gender'] == "FEMALE" ?  'selected="selected"': '' ?>>Female</option>
                                </select>  
                            
                            </div>

                            <div class="col-md-2">
                                <label for="child_age" class="form-label mt-3">Age</label>
                                <input type="text" class="form-control"  value="<?php echo isset($row['child_id']) ? $row['child_age'] : '' ?>" id="child_age">
                            </div>

                            <div class="col-md-4">
                                <label for="child_DOB" class="form-label mt-3">Date of Birth</label>
                                <input type="date" class="form-control"  value="<?php echo isset($row['child_id']) ? $row['child_DOB'] : '' ?>" id="child_DOB">
                            </div>

                            <div class="col-md-4">
                                <label for="child_religion" class="form-label mt-3">Religion</label>
                                <select id="child_religion" class="form-control">
                                    <option selected><?php echo isset($row["child_id"]) ? ucwords(strtolower($row["child_religion"]))  : 'Choose..' ?></option>
                                    <option>...</option>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label for="child_race" class="form-label mt-3">Race</label>
                                <select id="child_race" class="form-control">
                                    <option selected><?php echo isset($row["child_id"]) ? ucwords(strtolower($row["child_race"]))  : 'Choose..' ?></option>
                                    <option>...</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                                <label for="inputPassword4" class="form-label mt-3">Group</label>
                                <select id="group_id" name="group_id" class="form-control">
                                <option >Choose..</option>
                                <?php while($row1=$group->fetch_assoc()): ?>
                                <option value="<?php echo $row1['group_id'] ?>" <?php echo $row['group_id']== $row1['group_id']? 'selected':'' ?>><?php echo $row1['group_name'] ?></option>
                                <?php endwhile; ?>
                                </select>
                            </div>

                       


                            <!--parent part-->

                            <div class="col-md-12 mt-5">
                                <h5 class="text-dark "><u>Parent's Detail</u></h5>
                            </div>

                        
                            <div class="col-md-6">
                                <label for="parent_name" class="form-label mt-3">Father/Guardian Name</label>
                                <input type="text" class="form-control "  value="<?php echo isset($row['child_id']) ? $row['father_name'] : '' ?>" id="father_name">
                            </div>

                            
                            <div class="col-md-6">
                                <label for="parent_IC" class="form-label mt-3">Father/Guardian IC</label>
                                <input type="text" class="form-control "  value="<?php echo isset($row['child_id']) ? $row['father_IC'] : '' ?>" id="father_IC">
                            </div>

                            <?php if($row['child_registerStatus'] == 'WAITING'||$row['child_registerStatus'] == 'REJECT' ):?>

                            <div class="col-md-6">
                                <label for="parent_phoneNum" class="form-label mt-3"> Father/Guardian Phone Number</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['father_phoneNum'] : '' ?>" id="father_phoneNum">
                            </div>

                            <div class="col-md-6">
                                <label for="parent_work" class="form-label mt-3">Father/Guardian Work</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['father_work'] : '' ?>" id="father_work">
                            </div>
                            
                            <div class="col-md-12">
                                <label for="parent_workAddress" class="form-label mt-3">Father/Guardian Work Address</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['father_workAddress'] : '' ?>" id="father_workAddress">
                            </div>

                            <?php endif;?>

                            <div class="col-md-6">
                                <label for="parent_name" class="form-label mt-5">Mother Name</label>
                                <input type="text" class="form-control "  value="<?php echo isset($row['child_id']) ? $row['mother_name'] : '' ?>" id="mother_name">
                            </div>

                            
                            <div class="col-md-6">
                                <label for="parent_IC" class="form-label mt-5">Mother IC</label>
                                <input type="text" class="form-control "  value="<?php echo isset($row['child_id']) ? $row['mother_IC'] : '' ?>" id="mother_IC">
                            </div>

                            <?php if($row['child_registerStatus'] == 'WAITING'||$row['child_registerStatus'] == 'REJECT' ):?>

                            <div class="col-md-6">
                                <label for="parent_phoneNum" class="form-label mt-3"> Mother Phone Number</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['mother_phoneNum'] : '' ?>" id="mother_phoneNum">
                            </div>

                            <div class="col-md-6">
                                <label for="parent_work" class="form-label mt-3">Mother Work</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['mother_work'] : '' ?>" id="mother_work">
                            </div>

                            <div class="col-md-12">
                                <label for="parent_workAddress" class="form-label mt-3">Mother Work Address</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['mother_workAddress'] : '' ?>" id="mother_workAddress">
                            </div>

                            <?php endif;?>

                            <div class="col-md-12 mt-5">
                                <h5 class="text-dark "><u>Home Address's Detail</u></h5>
                            </div>
                    
                            <div class="col-md-12">
                                <label for="parent_address" class="form-label mt-3 ">Address</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['parent_address'] : '' ?>" id="parent_address">
                            </div>

                        
                            <div class="col-md-4">
                                <label for="parent_town" class="form-label mt-3">Town</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['parent_town'] : '' ?>" id="parent_town">
                            </div>

                            
                            <div class="col-md-4">
                                <label for="parent_postcode" class="form-label mt-3">Postcode</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['parent_postcode'] : '' ?>" id="parent_postcode">
                            </div>

                            
                            <div class="col-md-4">
                                <label for="parent_state" class="form-label mt-3">State</label>
                                <select id="parent_state" class="form-control">
                                    <option selected><?php echo isset($row['child_id']) ? $row['parent_state'] : '' ?></option>
                                    <option>...</option>
                                </select>
                            </div>

                            

                        



                            <!--Health part-->
                            <div class="col-md-12 mt-5">
                                <h5 class="text-dark "><u>Health's Detail</u></h5>
                            </div>

                            <div class="col-md-12">
                                <label for="health_allergic" class="form-label mt-3 ">Allergic</label>
                                <input type="text" class="form-control" value="<?php echo isset($row['child_id']) ? $row['health_allergic'] : '' ?>" id="health_allergic">
                            </div>

                            <div class="col-md-12">
                                <label for="health_information" class="form-label mt-3">Health Record</label>
                                <input type="file" class="form-control-file" value="<?php isset($row['child_id']) ? $row['health_information'] : '' ?>" id="health_information">
                                
                                <a href="download.php?id=<?php echo $row['child_id'] ?>"> download</a>
                            
                            </div>
                                                        

                            <!--Emergency part-->
                            <div class="col-md-12 mt-5">
                                <h5 class="text-dark "><u>Emergency Contact's Detail</u></h5>
                            </div>

                            <div class="col-md-6">
                                <label for="contact_name" class="form-label mt-3">Contact Name</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['contact_name'] : '' ?>" id="contact_name">
                            </div>

                            <div class="col-md-6">
                                <label for="contact_phoneNum" class="form-label mt-3">Contact Number</label>
                                <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['contact_phoneNum'] : '' ?>" id="contact_phoneNum">
                            </div>

                            <div class="col-md-12">
                                <label for="contact_relationship" class="form-label mt-3">Relationship with Children</label>
                                <select id="contact_relationship" class="form-control">
                                    <option selected> <?php echo isset($row['child_id']) ?ucwords(strtolower ($row['contact_relationship'])) : '' ?></option>
                                    <option>...</option>
                                </select>
                            </div>



                        


                
                    </div>
                    <div class="modal-footer">

                        <?php 

                            if ($row['child_registerStatus']=="WAITING"){

                            $id=$row['child_id'];       
                            echo "<button  class='btn btn-danger' type='button' data-dismiss='modal' href='#' data-toggle='modal' data-target='#deleteModal$id'>Reject</button>";
                            echo "<a href='handler/child_handler.php?status=ACCEPT&id=$id'class='btn btn-success' type='button' >Accept</a>";
                            } 

                            else{
                        
                                echo " <button class='btn btn-danger' type='button'>Delete</button>";
                                echo "<button class='btn btn-success' type='submit' data-dismiss='modal'>Save</button>" ;
                            }
                    ?>
                    </div>

                </form>
            
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if(!isset($row['child_id'])):?>

<div class="modal fade" id="childModal<?php echo isset($row['child_id']) ? $row['child_id'] : '' ?>" tabindex="-1" role="dialog" aria-labelledby="childInfoModal"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content" id="childDetail">
            
            <div class="modal-header"> <b></b>
                        <h5 class='modal-title' id='childInfoModal'>New Registration</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body m-4" >
                

                <form class="row g-2" action="">
                    <div class="col-md-12 text-center ">
                        <?php 
                        if (isset($row['child_id'])){
                        ?>
                        <img src="data:image/jpg;charset=utf8;base64,<?php echo base64_encode($row['child_image']); ?>" class="rounded mx-auto d-block mb-3" alt="..." width="200">
                        <?php }
                        
                        else{
                        ?>
                        <img src="../img/undraw_profile.svg" class="rounded mx-auto d-block mb-3" alt="..." width="200">
                        <?php }?>

                    </div>

                    <div class="col-md-11 my-4 ">
                        <label for="child_img">Children Picture</label>
                        <input type="file" class="form-control-file" id="child_img">
                    </div>
                    

                    <!--Child part-->
                    <div class="col-md-6">
                        <label for="child_name" class="form-label">Full Name</label>
                        <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['child_name'] : '' ?>" id="child_name">
                    </div>

                    <div class="col-md-6">
                        <label for="child_myKidNum" class="form-label ">MyKid Number</label>
                        <input type="text" class="form-control" value="<?php echo isset($row['child_id']) ? $row['child_myKidNum'] : '' ?>" id="child_myKidNum">
                    </div>

                    <div class="col-md-6">
                        <label for="child_nickname" class="form-label mt-3">Nickname</label>
                        <input type="text" class="form-control" value="<?php echo isset($row['child_id']) ? $row['child_nickname'] : '' ?>" id="child_nickname">
                    </div>

                    <div class=" col-md-4" >
                        
                        <label for="child_gender" class="form-label mt-3">Gender</label>
                        <select id="child_gender" class="form-control"  >
                            <option >Choose..</option>
                            <option value="MALE" <?php echo $row['child_gender'] == "MALE" ?  'selected="selected"': '' ?>>Male</option>
                            <option value="FEMALE" <?php echo $row['child_gender'] == "FEMALE" ?  'selected="selected"': '' ?>>Female</option>
                        </select>  
                    
                    </div>

                    <div class="col-md-2">
                        <label for="child_age" class="form-label mt-3">Age</label>
                        <input type="text" class="form-control"  value="<?php echo isset($row['child_id']) ? $row['child_age'] : '' ?>" id="child_age">
                    </div>

                    <div class="col-md-4">
                        <label for="child_DOB" class="form-label mt-3">Date of Birth</label>
                        <input type="date" class="form-control"  value="<?php echo isset($row['child_id']) ? $row['child_DOB'] : '' ?>" id="child_DOB">
                    </div>

                    <div class="col-md-4">
                        <label for="child_religion" class="form-label mt-3">Religion</label>
                        <select id="child_religion" class="form-control">
                            <option selected><?php echo isset($row["child_id"]) ? ucwords(strtolower($row["child_religion"]))  : 'Choose..' ?></option>
                            <option>...</option>
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label for="child_race" class="form-label mt-3">Race</label>
                        <select id="child_race" class="form-control">
                            <option selected><?php echo isset($row["child_id"]) ? ucwords(strtolower($row["child_race"]))  : 'Choose..' ?></option>
                            <option>...</option>
                        </select>
                    </div>

                    <!--parent part-->

                    <div class="col-md-12 mt-5">
                                <h5 class="text-dark "><u>Parent's Detail</u></h5>
                    </div>

                
                    <div class="col-md-6">
                        <label for="parent_name" class="form-label mt-3">Father/Guardian Name</label>
                        <input type="text" class="form-control "  value="<?php echo isset($row['child_id']) ? $row['father_name'] : '' ?>" id="father_name">
                    </div>

                    
                    <div class="col-md-6">
                        <label for="parent_IC" class="form-label mt-3">Father/Guardian IC</label>
                        <input type="text" class="form-control "  value="<?php echo isset($row['child_id']) ? $row['father_IC'] : '' ?>" id="father_IC">
                    </div>


                    <div class="col-md-6">
                        <label for="parent_phoneNum" class="form-label mt-3"> Father/Guardian Phone Number</label>
                        <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['father_phoneNum'] : '' ?>" id="father_phoneNum">
                    </div>

                    <div class="col-md-6">
                        <label for="parent_work" class="form-label mt-3">Father/Guardian Work</label>
                        <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['father_work'] : '' ?>" id="father_work">
                    </div>
                    
                    <div class="col-md-12">
                        <label for="parent_workAddress" class="form-label mt-3">Father/Guardian Work Address</label>
                        <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['father_workAddress'] : '' ?>" id="father_workAddress">
                    </div>


                    <div class="col-md-6">
                        <label for="parent_name" class="form-label mt-3">Mother Name</label>
                        <input type="text" class="form-control "  value="<?php echo isset($row['child_id']) ? $row['mother_name'] : '' ?>" id="mother_name">
                    </div>

                    
                    <div class="col-md-6">
                        <label for="parent_IC" class="form-label mt-3">Mother IC</label>
                        <input type="text" class="form-control "  value="<?php echo isset($row['child_id']) ? $row['mother_IC'] : '' ?>" id="mother_IC">
                    </div>


                    <div class="col-md-6">
                        <label for="parent_phoneNum" class="form-label mt-3"> Mother Phone Number</label>
                        <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['mother_phoneNum'] : '' ?>" id="mother_phoneNum">
                    </div>

                    <div class="col-md-6">
                        <label for="parent_work" class="form-label mt-3">Mother Work</label>
                        <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['mother_work'] : '' ?>" id="mother_work">
                    </div>

                    <div class="col-md-12">
                        <label for="parent_workAddress" class="form-label mt-3">Mother Work Address</label>
                        <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['mother_workAddress'] : '' ?>" id="mother_workAddress">
                    </div>


                    <div class="col-md-12 mt-5">
                        <h5 class="text-dark "><u>Home Address's Detail</u></h5>
                    </div>
            
                    <div class="col-md-12">
                        <label for="parent_address" class="form-label mt-3 ">Address</label>
                        <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['parent_address'] : '' ?>" id="parent_address">
                    </div>

                
                    <div class="col-md-4">
                        <label for="parent_town" class="form-label mt-3">Town</label>
                        <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['parent_town'] : '' ?>" id="parent_town">
                    </div>

                    
                    <div class="col-md-4">
                        <label for="parent_postcode" class="form-label mt-3">Postcode</label>
                        <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['parent_postcode'] : '' ?>" id="parent_postcode">
                    </div>

                    
                    <div class="col-md-4">
                        <label for="parent_state" class="form-label mt-3">State</label>
                        <select id="parent_state" class="form-control">
                            <option selected><?php echo isset($row['child_id']) ? $row['parent_state'] : '' ?></option>
                            <option>...</option>
                        </select>
                    </div>

                    

                



                    <!--Health part-->
                    <div class="col-md-12 mt-5">
                        <h5 class="text-dark "><u>Health's Detail</u></h5>
                    </div>

                    <div class="col-md-12">
                        <label for="health_allergic" class="form-label mt-3 ">Allergic</label>
                        <input type="text" class="form-control" value="<?php echo isset($row['child_id']) ? $row['health_allergic'] : '' ?>" id="health_allergic">
                    </div>

                    <div class="col-md-12">
                        <label for="health_information" class="form-label mt-3">Health Record</label>
                        <input type="file" class="form-control-file" value="<?php isset($row['child_id']) ? $row['health_information'] : '' ?>" id="health_information">
                        
                        <a href="download.php?id=<?php echo $row['child_id'] ?>"> download</a>
                    
                    </div>
                                                

                    <!--Emergency part-->
                    <div class="col-md-12 mt-5">
                        <h5 class="text-dark "><u>Emergency Contact's Detail</u></h5>
                    </div>

                    <div class="col-md-6">
                        <label for="contact_name" class="form-label mt-3">Contact Name</label>
                        <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['contact_name'] : '' ?>" id="contact_name">
                    </div>

                    <div class="col-md-6">
                        <label for="contact_phoneNum" class="form-label mt-3">Contact Number</label>
                        <input type="text" class="form-control " value="<?php echo isset($row['child_id']) ? $row['contact_phoneNum'] : '' ?>" id="contact_phoneNum">
                    </div>

                    <div class="col-md-12">
                        <label for="contact_relationship" class="form-label mt-3">Relationship with Children</label>
                        <select id="contact_relationship" class="form-control">
                            <option selected> <?php echo isset($row['child_id']) ?ucwords(strtolower ($row['contact_relationship'])) : '' ?></option>
                            <option>...</option>
                        </select>
                    </div>



                </form>


        
            </div>
            <div class="modal-footer">

                <button class='btn btn-success' type='button' data-dismiss='modal'>Create</button>;
                
            </div>
        
        </div>
    </div>
</div>

<?php endif; ?>

<div class="modal align-middle fade" id="deleteModal<?php echo isset($row['child_id']) ? $row['child_id'] : '' ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="handler/child_handler.php" method='POST'>
                    <div class="modal-body  my-3">

                        <div class="my-2">
                            <h4> Please State the Reason of Rejection </h4>  
                            <textarea type="text" class="form-control" id="inputPassword4" rows="3" name="child_rejectReason"></textarea>
                            <input type="hidden" class="form-control" id="inputPassword4" name="registerStatus" value="REJECT">
                            <input type="hidden" class="form-control" id="inputPassword4" name="id" value="<?php echo $row['child_id']  ?>">
                        </div>

                    </div>

                    <div class='modal-footer'>
                    

                        <button  class='btn btn-danger' type='submit' >  Reject</button>
                        <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">Cancel</button>

                    </div>

                </form>
                
                
            </div>
        </div>
</div>
