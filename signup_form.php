<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nursery Management System</title>

   
    <?php 
    session_start();
    include 'header.php';

    ?>
  
    <link rel="stylesheet" href="style.css?v=<?php echo time(); ?>">

</head>
<body class="vh-100 gradient-custom ">


    <div class="container" >

        <div class="row align-items-center vh-100 ">
            <div class="card o-hidden border-0 shadow-lg  my-5  ">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Create an Account! <?php echo $_SESSION['signup'];?></h1>
                        </div>

                

                        <form class="user " action="signup_handler.php" method="POST">

                            <nav hidden>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">

                                        <a class="nav-item nav-link active" id="nav-signin-tab" data-toggle="tab" href="#nav-signin" role="tab" aria-controls="nav-signin" aria-selected="true" style="color:gray;">Father/Guardian's Details</a>

                                        <a class="nav-item nav-link " id="nav-father-tab" data-toggle="tab" href="#nav-father" role="tab" aria-controls="nav-father" aria-selected="true" style="color:gray;">Father/Guardian's Details</a>
                                    
                                    
                                        <a class="nav-item nav-link" id="nav-mother-tab" data-toggle="tab" href="#nav-mother" role="tab" aria-controls="nav-mother" aria-selected="false" style="color:gray;">Mother's Details</a>
                                    
                                    
                                        <a class="nav-item nav-link" id="nav-address-tab" data-toggle="tab" href="#nav-address" role="tab" aria-controls="nav-address" aria-selected="false" style="color:gray;">Home Address</a>
                                    

                                </div>
                            </nav>

                            <div class="tab-content" id="nav-tabContent">

                                <div class="tab-pane fade  show active" id="nav-signin" role="tabpanel" aria-labelledby="nav-signin-tab">

                                        <div class="form-group row">
                                            <div class="col-md-12 mt-5">
                                            
                                                <h5>User's Detail</h5>
                                                <hr style="width:120; height:0.3px;">
                                            
                                            </div>
                                        </div>

                                        <div class="form-group row">

                                            <label for="staticEmail" class="col-md-3 col-form-label" >Username</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="username" required="true">
                                            </div>                                        
                                        </div>

                                        <div class="form-group row">

                                            <label for="staticEmail" class="col-md-3 col-form-label">Password</label>
                                            <div class="col-md-9">
                                                <input type="password" class="form-control" name="password" required="true">                                        
                                            </div>

                                        </div>
                                        

                                </div>


                                <div class="tab-pane fade" id="nav-father" role="tabpanel" aria-labelledby="nav-father-tab">

                                    <div class="form-group row">
                                        <div class="col-md-12 mt-5">
                                        
                                            <h5>Father/Guardian's Details </h5>
                                            <hr style="width:230; height:0.3px;">
                                        
                                        </div>
                                    </div>
                                    

                                    <div class="form-group row">

                                        <label for="staticEmail" class="col-md-3 col-form-label">Full Name</label>
                                        <div class="col-md-9">
                                                <input type="text" class="form-control" name="father_name" required="true">
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="staticEmail" class="col-md-3 col-form-label">Identity Card Number</label>
                                        <div class="col-md-9">
                                                <input type="text" class="form-control " name="father_ic_signup" value="<?php echo isset($_GET['father_ic'])?  $_GET['father_ic']:''?>" required="true">
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="staticEmail" class="col-md-3 col-form-label">Phone Number</label>
                                        <div class="col-md-9">
                                                <input type="text" class="form-control" name="father_phoneNum">
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="staticEmail" class="col-md-3 col-form-label">Work</label>
                                        <div class="col-md-9">
                                                <input type="text" class="form-control" name="father_work">
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="staticEmail" class="col-md-3 col-form-label">Work Address</label>
                                        <div class="col-md-9">
                                                <textarea type="text" class="form-control " id="inputPassword4" rows="3" name="father_workAddress"></textarea>
                                        </div>

                                    </div>

                                    
                                </div>

                                <div  class="tab-pane fade " id="nav-mother" role="tabpanel" aria-labelledby="nav-mother-tab">

                                    <div class="form-group row">
                                        <div class="col-md-12 mt-5">
                                        
                                            <h5>Mother's Details </h5>
                                            <hr style="width:147; height:0.3px;">
                                        
                                        </div>
                                    </div>
                                    

                                    <div class="form-group row">

                                        <label for="staticEmail" class="col-md-3 col-form-label">Full Name</label>
                                        <div class="col-md-9">
                                                <input type="text" class="form-control" name="mother_name" required="true">
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="staticEmail" class="col-md-3 col-form-label">Identity Card Number</label>
                                        <div class="col-md-9">
                                                <input type="text" class="form-control " name="mother_ic_signup" value="<?php echo isset($_GET['mother_ic'])?  $_GET['mother_ic']:''?>" required="true">
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="staticEmail" class="col-md-3 col-form-label">Phone Number</label>
                                        <div class="col-md-9">
                                                <input type="text" class="form-control" name="mother_phoneNum">
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="staticEmail" class="col-md-3 col-form-label">Work</label>
                                        <div class="col-md-9">
                                                <input type="text" class="form-control" name="mother_work">
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="staticEmail" class="col-md-3 col-form-label">Work Address</label>
                                        <div class="col-md-9">
                                                <textarea type="text" class="form-control " id="inputPassword4" rows="3" name="mother_WorkAddress"></textarea>
                                        </div>

                                    </div>

                                    
                                </div>

                                
                                <div class="tab-pane fade" id="nav-address" role="tabpanel" aria-labelledby="nav-address-tab">

                                    <div class="form-group row">
                                        <div class="col-md-12 mt-5">
                                        
                                            <h5>Home Address</h5>
                                            <hr style="width:145; height:0.3px;">
                                        
                                        </div>
                                    </div>
                                    

                                    <div class="form-group row">

                                        <label for="staticEmail" class="col-md-3 col-form-label">Address</label>
                                        <div class="col-md-9">
                                            <textarea type="text" class="form-control " id="inputPassword4" rows="3" name="parent_address"></textarea>
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="staticEmail" class="col-md-3 col-form-label">Town</label>
                                        <div class="col-md-9">
                                                <input type="text" class="form-control " name="parent_town" >
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="staticEmail" class="col-md-3 col-form-label">Postcode</label>
                                        <div class="col-md-9">
                                                <input type="text" class="form-control" name="parent_postcode">
                                        </div>

                                    </div>

                                    <div class="form-group row">

                                        <label for="staticEmail" class="col-md-3 col-form-label">State</label>
                                        <div class="col-md-9">
                                                <input type="text" class="form-control" name="parent_state">
                                        </div>

                                    </div>
                                    
                                   
                                    
                                </div>
                            </div>

                            <button class="btn btn-success float-right mt-3 " type="submit">Submit</button>
                            <a href="index.php"class="btn btn-secondary float-right mt-3 me-2" >Cancel</a>
 
                        </form>

                  

                            <button class="prevtab btn btn-outline-secondary mt-3">Prev</button>
                        
                            <button class="nexttab btn btn-outline-secondary mt-3">Next</button>

                      
                    </div>

                            
                </div>

               
                    
               
            </div>
        </div>
    </div>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script>
    function bootstrapTabControl(){
    var i, items = $('.nav-link'), pane = $('.tab-pane');
    // next
    $('.nexttab').on('click', function(){
        for(i = 0; i < items.length; i++){
            if($(items[i]).hasClass('active') == true){
                break;
            }
        }
        if(i < items.length - 1){
            // for tab
            $(items[i]).removeClass('active');
            $(items[i+1]).addClass('active');
            // for pane
            $(pane[i]).removeClass('show active');
            $(pane[i+1]).addClass('show active');
        }

    });
    // Prev
    $('.prevtab').on('click', function(){
        for(i = 0; i < items.length; i++){
            if($(items[i]).hasClass('active') == true){
                break;
            }
        }
        if(i != 0){
            // for tab
            $(items[i]).removeClass('active');
            $(items[i-1]).addClass('active');
            // for pane
            $(pane[i]).removeClass('show active');
            $(pane[i-1]).addClass('show active');
        }
    });
    }
    bootstrapTabControl();


</script>




</body>
</html>