<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nursery Management System</title>
   
    <?php 
    session_start();
 
    $_SESSION['signup']="0";
     include 'header.php';?>
  
    <link rel="stylesheet" href="style.css?v=<?php echo time(); ?>">

</head>
<body >


<section class="vh-100 gradient-custom s">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100 ">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5">
        <div class="card bg-white text-black shadow-lg bg-body" style="border-radius: 0.5rem;">

    

            <!--sing in form-->
            
          <div class="card-body p-5 text-left signin_form " >

            <div class="p-4 text-center signin_form">
            <img src="image/logo1.png" width="300" >
            </div>
            <div >
                <?php 

                if(@$_GET['alert']==true)
                {
                ?>
                    <div class="alert alert-danger  block  alert-dismissible" >
                      
                            <button type="button" class="close mt-3" data-bs-dismiss="alert">
                                    <span aria-hidden="true">&times;</span>
                            </button>

                            <div>
                            <p class="text-justify font-weight-light mt-3 text-xl-left" width="90";><?php echo $_GET['alert']?></p>
                            </div>
                    </div>
                <?php }?>
              </div>
         
            <form class="user" method="POST" action="signin_handler.php" id="signin-form">  

              <div class="mb-3">
              <input type="hidden" class="form-control form-control-user"  name="role"  value="<?php echo $_GET['role'] ?>" >
                <input type="username" class="form-control form-control-user" name="username" placeholder="Username" >
              </div>

              <div class="mb-3">
              
              <input type="password" class="form-control form-control-user" name="password" placeholder="Password" >
              </div>
              
              <div class="container" >

                <div class="row">

                  <div class="col">
                  <button type="submit" class="btn btn-secondary btn-user btn-block ">Sign In</button>
                  </div>

                  <div class="col">
                  <a  class="btn btn-outline-dark btn-user btn-block" href="index.php">Cancel</a>
                  </div>

                </div>
              </div>
            </form>
            <?php if($_GET['role']=="PARENT"):?>
              <hr class="sidebar-divider" style="height:0.3px;">

              <div class="text-center">
                  <a class="text-secondary small" href="forgot-password.html">Forgot Password?</a>
              </div>

              
              <div class="text-center p-1 ">
                <a  href="signup.php" class="text-secondary small "> Create New Account!</a>
              </div>
              <?php endif ?>
            

          </div>
       


        </div>
      </div>
    </div>
  </div>
</section>
        



</body>
</html>